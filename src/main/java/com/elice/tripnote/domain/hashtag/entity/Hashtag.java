package com.elice.tripnote.domain.hashtag.entity;

import com.elice.tripnote.global.entity.BaseTimeEntity;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import java.time.LocalDateTime;

@Entity
@Table(name = "hashtag")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Hashtag extends BaseTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    //0 - false
    //1 - true
    @Column(name = "is_city", columnDefinition = "TINYINT(1)")
    @ColumnDefault("false")
    private boolean isCity;



    @Builder
    public Hashtag(String name, boolean isCity){
        this.name = name;
        this.isCity = isCity;
    }

    public void update(HashtagRequestDTO hashtagRequestDTO){
        this.name = hashtagRequestDTO.getName();
        this.isCity = hashtagRequestDTO.isCity();
    }

    //true -> false, false -> true
    //삭제된 해시태그를 복구할 수 있게
    public void delete(){
        if(deletedAt != null){
            deletedAt = null;
            return;
        }
        deletedAt = LocalDateTime.now();
    }

    public HashtagResponseDTO toResponseDTO() {
        return HashtagResponseDTO.builder()
                .id(id)
                .name(name)
                .isCity(isCity)
                .build();
    }

}
