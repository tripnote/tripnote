package com.elice.tripnote.domain.notification.repository;

import com.elice.tripnote.domain.notification.entity.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, Long>, CustomNotificationRepository {
}
