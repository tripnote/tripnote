package com.elice.tripnote.domain.notification.service;


import com.elice.tripnote.domain.comment.entity.Comment;
import com.elice.tripnote.domain.member.repository.MemberRepository;
import com.elice.tripnote.domain.notification.entity.Notification;
import com.elice.tripnote.domain.notification.entity.NotificationDTO;
import com.elice.tripnote.domain.notification.entity.NotificationMessage;
import com.elice.tripnote.domain.notification.repository.NotificationRepository;
import com.elice.tripnote.domain.notification.repository.SseEmitterRepository;
import com.elice.tripnote.domain.post.entity.Post;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class NotificationService {
    private final NotificationRepository notificationRepository;
    private final MemberRepository memberRepository;
    private final SseEmitterRepository sseEmitterRepository;


    @Async
    public SseEmitter subscribe() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        String emitterId = email + "_" + System.currentTimeMillis();

        //시간 설정 = 10분
        SseEmitter emitter = sseEmitterRepository.save(emitterId, new SseEmitter(10 * 60 * 1000L));

        emitter.onCompletion(() -> sseEmitterRepository.deleteById(emitterId));
        emitter.onTimeout(() -> sseEmitterRepository.deleteById(emitterId));

        // 503 에러를 방지하기 위한 더미 이벤트 전송
        sendToClient(
                emitter,
                emitterId,
                NotificationMessage.builder()
                        .type("subscribe")
                        .data("EventStream Created. [userId=" + email + "]")
                        .build()
        );

        sendNotifications(email, emitter, emitterId);


        return emitter;
    }

    public void sendNotification(Post post, Comment comment) {
        Notification newNotification = Notification.builder()
                .post(post)
                .comment(comment)
                .build();
        notificationRepository.save(newNotification);

        String email = post.getMember().getEmail();
        Map<String, SseEmitter> emitters = sseEmitterRepository.findAllEmitterStartWithByEmail(email);

        if(emitters.isEmpty()){
            return;
        }

        NotificationMessage message = NotificationMessage.builder()
                .type("single")
                .data(NotificationDTO.builder().postId(post.getId()).newCommentCounts(1L).build())
                .build();

        emitters.forEach(
                (key, emitter) -> {
                    sendToClient(
                            emitter,
                            key,
                            message
                    );
                }
        );
    }

    public void checkNotification(Long postId) {

        notificationRepository.customCheckNotification(postId);

    }


    private void sendNotifications(String email, SseEmitter emitter, String emitterId) {

        List<NotificationDTO> notificationDTOList = notificationRepository.customFindNotReadNotification(email);

        if(notificationDTOList.isEmpty()){
            return;
        }

        sendToClient(
                emitter,
                emitterId,
                NotificationMessage.builder()
                        .type("list")
                        .data(notificationDTOList)
                        .build()
        );

    }



    private void sendToClient(SseEmitter emitter, String emitterId, Object data) {
        try {
            emitter.send(SseEmitter.event()
                    .id(emitterId)
                    .name("sse")
                    .data(data));
        } catch (IOException exception) {
            sseEmitterRepository.deleteById(emitterId);
        }
    }

}
