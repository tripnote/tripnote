package com.elice.tripnote.domain.notification.controller;


import com.elice.tripnote.domain.notification.service.NotificationService;
import com.elice.tripnote.global.annotation.MemberRole;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/member/notifications/subscribe")
public class NotificationController {

    private final NotificationService notificationService;

    @MemberRole
    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter subscribe(){
        return notificationService.subscribe();
    }

}
