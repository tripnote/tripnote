package com.elice.tripnote.domain.notification.repository;


import com.elice.tripnote.domain.comment.entity.QComment;
import com.elice.tripnote.domain.member.entity.QMember;
import com.elice.tripnote.domain.notification.entity.NotificationDTO;
import com.elice.tripnote.domain.notification.entity.QNotification;
import com.elice.tripnote.domain.post.entity.QPost;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@RequiredArgsConstructor
public class CustomNotificationRepositoryImpl implements CustomNotificationRepository{



    private final JPAQueryFactory query;

    private final QNotification notification = QNotification.notification;
    private final QComment comment = QComment.comment;
    private final QPost post = QPost.post;
    private final QMember member = QMember.member;


    public List<NotificationDTO> customFindNotReadNotification(String email){



        List<NotificationDTO> NotificationDTOList = query
                .select(Projections.constructor(NotificationDTO.class,
                        post.id,
                        comment.count()
                ))
                .from(post)
                .join(post.member, member)
                .on(member.email.eq(email))
                .join(post.Comments, comment)
                .join(comment.notification, notification)
                .where(post.deletedAt.isNull()
                        .and(comment.deletedAt.isNull())
                        .and(notification.isRead.isFalse()))
                .groupBy(post.id)
                .fetch();

        return NotificationDTOList;
    }

    @Override
    public void customCheckNotification(Long postId) {
        query
                .update(notification)
                .set(notification.isRead, true)
                .where(notification.post.id.eq(postId))
                .execute();
    }


}
