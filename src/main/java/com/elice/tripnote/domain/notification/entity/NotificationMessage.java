package com.elice.tripnote.domain.notification.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationMessage {


    private String type;

    //if type = subscribe, data = String
    //if type = single, data = NotificationDTO
    //if type = list, data = List<NotificationDTO> NotificationDTOList
    private Object data;
}
