package com.elice.tripnote.domain.notification.repository;

import com.elice.tripnote.domain.notification.entity.NotificationDTO;

import java.util.List;

public interface CustomNotificationRepository {

    List<NotificationDTO> customFindNotReadNotification(String email);

    void customCheckNotification(Long postId);
}
