package com.elice.tripnote.domain.link.routehashtag.repository;

import com.elice.tripnote.domain.link.routehashtag.entity.RouteHashtag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RouteHashtagRepository extends JpaRepository<RouteHashtag, Long> {
//    List<RouteHashtag> findByIntegratedRoute_IntegratedRoutes(String integratedRoutes);


//    @Query("SELECT uh.hashtag.id " +
//            "FROM RouteHashtag uh " +
//            "JOIN IntegratedRoute ir ON ir.id=uh.integratedRoute.id " +
//            "WHERE ir.id = :integratedRouteId")
//    List<Long> findHashtagIdsByIntegratedRouteId(@Param("integratedRouteId") Long integratedRouteId);
}
