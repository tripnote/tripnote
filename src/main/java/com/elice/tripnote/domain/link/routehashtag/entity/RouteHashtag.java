package com.elice.tripnote.domain.link.routehashtag.entity;

import com.elice.tripnote.domain.hashtag.entity.Hashtag;
import com.elice.tripnote.domain.route.entity.Route;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "route_hashtag")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class RouteHashtag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

//    @Column(name = "hashtag_id", nullable = false)
//    Long hashtag_id;

    @ManyToOne(optional = true)
    @JoinColumn(name = "hashtag_id", nullable = false)
    Hashtag hashtag;

    @ManyToOne
    @JoinColumn(name = "route_id", nullable = false)
    Route route;

    @Builder
    public RouteHashtag(Hashtag hashtag, Route route) {
        this.hashtag = hashtag;
        this.route = route;
    }
}
