package com.elice.tripnote.domain.link.favorite.repository;

import com.elice.tripnote.domain.link.favorite.entity.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite, Long> {

    Favorite findByPostIdAndMemberId(Long postId, Long memberId);
}
