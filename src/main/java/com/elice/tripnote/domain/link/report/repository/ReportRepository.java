package com.elice.tripnote.domain.link.report.repository;

import com.elice.tripnote.domain.link.report.entity.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {

    Report findByPostIdAndMemberId(Long postId, Long memberId);

    Report findByCommentIdAndMemberId(Long commentId, Long memberId);


}
