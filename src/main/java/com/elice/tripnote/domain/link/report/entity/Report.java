package com.elice.tripnote.domain.link.report.entity;

import com.elice.tripnote.domain.comment.entity.Comment;
import com.elice.tripnote.domain.member.entity.Member;
import com.elice.tripnote.domain.post.entity.Post;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Table(name="report")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column
    private LocalDateTime reportedAt;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id", nullable = true)
    private Post post;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comment_id", nullable = true)
    private Comment comment;


    public void report(){


        if(comment !=null){
            if(reportedAt == null){
                reportedAt = LocalDateTime.now();
                comment.addReport();
                return;
            }
            reportedAt = null;
            comment.removeReport();
            return;
        }

        if(post != null){
            if(reportedAt == null){
                reportedAt = LocalDateTime.now();
                post.addReport();
                return;
            }
            reportedAt = null;
            post.removeReport();
        }
    }





}
