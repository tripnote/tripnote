package com.elice.tripnote.domain.route.repository;

import com.elice.tripnote.domain.route.entity.RecommendedRouteResponseDTO;
import com.elice.tripnote.domain.route.entity.RouteDetailResponseDTO;
import com.elice.tripnote.domain.spot.constant.Region;
import com.elice.tripnote.global.entity.PageRequestDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CustomRouteRepository {
    int getRouteLikeCounts(Long routeId);
    boolean findHashtagIdIdCity(Long hashtagId);
    Page<RouteDetailResponseDTO> findRouteDetailsByMemberId(Long memberId, PageRequestDTO pageRequestDTO, boolean isBookmark);
    List<RecommendedRouteResponseDTO> getRecommendedRoutes(List<Long> routeIds, Long memberId, boolean isMember);
    List<Long> findIntegratedRouteIdsBySpotsAndLikes(List<Long> spots);
    boolean existsByMemberIdAndRouteId(Long memberId, Long routeId, boolean isLike);
    void deleteByMemberIdAndRouteId(Long memberId, Long routeId, boolean isLike);
    List<Long> findTopRoutesByRegion(Region region);
//    void countLikesLast3Months(Long routeId);


}
