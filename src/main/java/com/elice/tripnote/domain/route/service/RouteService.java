package com.elice.tripnote.domain.route.service;

import com.elice.tripnote.domain.hashtag.entity.Hashtag;
import com.elice.tripnote.domain.hashtag.repository.HashtagRepository;
import com.elice.tripnote.domain.link.bookmark.entity.Bookmark;
import com.elice.tripnote.domain.link.bookmark.repository.BookmarkRepository;
import com.elice.tripnote.domain.link.favorite.entity.Favorite;
import com.elice.tripnote.domain.link.favorite.repository.FavoriteRepository;
import com.elice.tripnote.domain.link.routehashtag.entity.RouteHashtag;
import com.elice.tripnote.domain.link.routehashtag.repository.RouteHashtagRepository;
import com.elice.tripnote.domain.link.routespot.entity.RouteSpot;
import com.elice.tripnote.domain.link.routespot.repository.RouteSpotRepository;
import com.elice.tripnote.domain.member.entity.Member;
import com.elice.tripnote.domain.member.repository.MemberRepository;
import com.elice.tripnote.domain.post.repository.PostRepository;
import com.elice.tripnote.domain.route.entity.*;
import com.elice.tripnote.domain.route.repository.RouteRepository;
import com.elice.tripnote.domain.spot.constant.Region;
import com.elice.tripnote.domain.spot.entity.Spot;
import com.elice.tripnote.domain.spot.repository.SpotRepository;
import com.elice.tripnote.global.entity.PageRequestDTO;
import com.elice.tripnote.global.exception.CustomException;
import com.elice.tripnote.global.exception.ErrorCode;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class RouteService {
    private final RouteRepository routeRepository;

    private final RouteHashtagRepository routeHashtagRepository;
    private final RouteSpotRepository routeSpotRepository;

    private final HashtagRepository hashtagRepository;
    private final MemberRepository memberRepository;
    private final SpotRepository spotRepository;
    private final BookmarkRepository bookmarkRepository;
    private final FavoriteRepository favoriteRepository;
    private final PostRepository postRepository;

    @Transactional
    public Long save(SaveRequestDTO requestDto) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("유저 이메일: {}", email);
        //여행지 id 리스트 기반으로 uuid 만들기
        String uuid = generateUUID(requestDto.getSpotIds());

        Region region = findRegionOfSpots(requestDto.getSpotIds());

        if (region != Region.MIXED_REGION) {
            Long regionHashtagId = (long) region.getIndex() + 1; //서울이 2번 해시태그로 들어감
            log.info("지역 해시태그 아이디: {}", regionHashtagId);
            if (routeRepository.findHashtagIdIdCity(regionHashtagId)) requestDto.getHashtagIds().add(regionHashtagId);
        }

        // route 객체 생성 -> 경로 저장
        Member member = memberRepository.findByEmail(email).orElseThrow(() -> {
            throw new CustomException(ErrorCode.NO_MEMBER);
        });
        Route route = Route.builder()
                .member(member)
                // expense 값이 안들어왔다면 0으로 초기화
//                .expense(requestDto.getExpense() != 0 ? requestDto.getExpense() : 0)
                .integratedRoutes(uuid)
                .name(requestDto.getName())
                .region(region)
//                .likesLast3Months(0)
                .build();
        route = routeRepository.save(route);

        List<Hashtag> hashtags = requestDto.getHashtagIds().stream()
                .map(hashtagRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        for (Hashtag hashtag : hashtags) {
            RouteHashtag uuidHashtag = RouteHashtag.builder()
                    .hashtag(hashtag)
                    .route(route)
                    .build();
            routeHashtagRepository.save(uuidHashtag);
        }

        // route_spot 객체 생성
        List<Long> spotIds = requestDto.getSpotIds();
        for (int i = 0; i < spotIds.size(); i++) {
            Spot spot = spotRepository.findById(spotIds.get(i))
                    .orElseThrow(() -> {
                        throw new CustomException(ErrorCode.NO_SPOT);
                    });
            Long nextSpotId = (i + 1 < spotIds.size()) ? spotIds.get(i + 1) : null;
            RouteSpot routeSpot = RouteSpot.builder()
                    .route(route)
                    .spot(spot)
                    .sequence(i + 1)
                    .nextSpotId(nextSpotId)
                    .build();
            routeSpotRepository.save(routeSpot);
        }

        return route.getId();
    }

    private Region findRegionOfSpots(List<Long> spotIds) {
        Region region = spotRepository.getRegionByspotId(spotIds.get(0)).getRegion();
        for (Long id : spotIds) {
            if (region != spotRepository.getRegionByspotId(id).getRegion()) return Region.MIXED_REGION;
        }
        return region;
    }


    //여행지 id 리스트를 매개변수로 전달
    private static String generateUUID(List<Long> ids) {
        try {
            // 식별자들을 문자열로 변환하고 결합
            StringBuilder combined = new StringBuilder();
            for (Long id : ids) {
                combined.append(id.toString());
            }

            // SHA-1 해시 생성
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] hash = md.digest(combined.toString().getBytes(StandardCharsets.UTF_8));

            // 해시의 앞 16 바이트를 사용해 UUID 생성
            long msb = 0;
            long lsb = 0;
            for (int i = 0; i < 8; i++) {
                msb = (msb << 8) | (hash[i] & 0xff);
            }
            for (int i = 8; i < 16; i++) {
                lsb = (lsb << 8) | (hash[i] & 0xff);
            }

            return new UUID(msb, lsb).toString();
        } catch (NoSuchAlgorithmException e) {
            throw new CustomException(ErrorCode.NOT_FOUND_ALGORITHM);
        }
    }

//    @Transactional
//    public Long setRouteToStatus(Long routeId) {
//        Member member = getMemberFromJwt();
//
//        Route route = routeRepository.findById(routeId)
//                .orElseThrow(() -> {
//                    throw new CustomException(ErrorCode.NO_ROUTE);
//                });
//
//        // 해당 경로가 자신의 것이 맞는지 확인
//        if (member.getId() != route.getMember().getId())
//            throw new CustomException(ErrorCode.UNAUTHORIZED_UPDATE_STATUS);
//        if (route.getRouteStatus() == RouteStatus.PUBLIC) route.updateStatus(RouteStatus.PRIVATE);
//        else if (route.getRouteStatus() == RouteStatus.PRIVATE) route.updateStatus(RouteStatus.PUBLIC);
//        else {
//            log.warn("삭제된 경로입니다. 경로 번호: {}", routeId);
//            throw new CustomException(ErrorCode.NO_ROUTE);
//        }
//
//        return routeRepository.save(route).getId();
//    }

    @Transactional
    public Long deleteRoute(Long routeId) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        Member member = memberRepository.findByEmail(email).orElseThrow(() -> {
            throw new CustomException(ErrorCode.NO_MEMBER);
        });

        log.info("{}번 경로가 삭제됩니다.", routeId);
        Route route = routeRepository.findById(routeId)
                .orElseThrow(() -> {
                    throw new CustomException(ErrorCode.NO_ROUTE);
                });

        // 해당 경로가 자신의 것이 맞는지 확인
        if (member.getId() != route.getMember().getId()) throw new CustomException(ErrorCode.UNAUTHORIZED_DELETE);

        route.deleteRoute();
        route = routeRepository.save(route);

        return route.getId();
    }

    public RecommendedRouteResponseDTO getRouteInfo(Long routeId) {
        Member member = getMemberFromJwt();

        log.info("통과");
        return RecommendedRouteResponseDTO.builder()
                .routeId(routeId)
                .postId(
                        postRepository.findByRouteId(routeId)
                                .map(post -> post.getId())
                                .orElse(null)
                )
                .spots(spotRepository.findSpotsByrouteIdInOrder(routeId)) // 해당 route에 맞는 spots구하기
                .likes(routeRepository.getRouteLikeCounts(routeId)) // 해당 경로의 좋아요 수
                .likedAt(routeRepository.existsByMemberIdAndRouteId(member.getId(), routeId, true)) // 자신이 이 경로에 좋아요를 눌렀는지
                .markedAt(routeRepository.existsByMemberIdAndRouteId(member.getId(), routeId, false)) // 자신이 이 경로에 북마크를 눌렀는지
                .build();
    }

    public List<RecommendedRouteResponseDTO> getRegionMember(Region region) {
        Member member = getMemberFromJwt();
        return getRegion(region, true, member);
    }

    public List<RecommendedRouteResponseDTO> getRegionGuest(Region region) {
        return getRegion(region, false, null);
    }

    private List<RecommendedRouteResponseDTO> getRegion(Region region, boolean isMember, Member member) {
        //통합 경로로 묶어서 좋아요 탑5 추려낸 다음, 그룹에서 가장 작은 route id 가져옴
        List<Long> routeIds = routeRepository.findTopRoutesByRegion(region);
        log.info("리턴되는 경로 id: {}", routeIds);
        return getRecommendRoutesByIntegratedRoutes(routeIds, isMember, member);
    }

    private List<RecommendedRouteResponseDTO> getRecommendRoutesByIntegratedRoutes(List<Long> routeIds, boolean isMember, Member member) {
        return routeRepository.getRecommendedRoutes(routeIds, isMember ? member.getId() : null, isMember);
    }


    // 게시물에서 경로 표현할 때 사용?
    public List<SpotResponseDTO> getSpots(Long routeId) {
        return spotRepository.findByRouteIds(routeId);
    }

    public List<RecommendedRouteResponseDTO> getRoutesThroughSpotMember(List<Long> spots) {
        Member member = getMemberFromJwt();
        return getRoutesThroughSpot(spots, true, member);
    }

    public List<RecommendedRouteResponseDTO> getRoutesThroughSpotGuest(List<Long> spots) {
        return getRoutesThroughSpot(spots, false, null);
    }

    private List<RecommendedRouteResponseDTO> getRoutesThroughSpot(List<Long> spots, boolean isMember, Member member) {

        /*
        1. 통합 경로의 여행지에 spots가 모두 포함되는거만 필터링
        2. 통합 경로 id 중, 해시태그_uuid_연결 테이블의 hashtag_id에 hashtags 값이 모두 있는 애들 필터링
        3. 이렇게 나온 통합 경로 id와 기간별_좋아요_북마크 join해서 기간별 좋아요 수를 기준으로 통합 경로 id를 정렬한다
        4. 이렇게 정렬한 것 중 상위 5개의 통합 경로 id를 리턴한다
         */

        // 먼저 경로 중, spots가 모두 포함되는 거 찾기
        // 그리고 같은 통합 경로 id를 가진 애들끼리 묶어서 통합 경로 id 리턴 (ids로)

        // 해당하는 여행지들이 모두 있는 통합 경로 id 리턴

        List<Long> routeIds = routeRepository.findIntegratedRouteIdsBySpotsAndLikes(spots);
        log.info("리턴되는 통합 경로 id: {}", routeIds);

        return getRecommendRoutesByIntegratedRoutes(routeIds, isMember, member);
    }

    @Transactional
    public void addOrRemoveLike(Long routeId) {
        Member member = getMemberFromJwt();

        if (routeRepository.existsByMemberIdAndRouteId(member.getId(), routeId, true)) {
            log.info("통합경로 {}번 경로의 좋아요를 취소하겠습니다.", routeId);
            routeRepository.deleteByMemberIdAndRouteId(member.getId(), routeId, true);
        }else{
            // 없는 경우
            log.info("통합경로 {}번 경로에 좋아요를 추가하겠습니다.", routeId);
            Route route = routeRepository.findById(routeId)
                    .orElseThrow(() -> {
                        throw new CustomException(ErrorCode.NO_ROUTE);
                    });

            Favorite likePost = Favorite.builder()
                    .member(member)
                    .route(route)
                    .likedAt(LocalDateTime.now())
                    .build();
            favoriteRepository.save(likePost);
        }
        //좋아요 개수를 새어서 likes 변수에 대입
//        routeRepository.countLikesLast3Months(routeId);
    }

    @Transactional
    public void addOrRemoveBookmark(Long routeId) {
        Member member = getMemberFromJwt();

        if (routeRepository.existsByMemberIdAndRouteId(member.getId(), routeId, false)) {
            log.info("통합경로 {}번 경로의 북마크를 취소하겠습니다.", routeId);
            routeRepository.deleteByMemberIdAndRouteId(member.getId(), routeId, false);
            return;
        }

        // 없는 경우
        log.info("통합경로 {}번 경로에 북마크를 추가하겠습니다.", routeId);
        Route route = routeRepository.findById(routeId)
                .orElseThrow(() -> {
                    throw new CustomException(ErrorCode.NO_ROUTE);
                });

        Bookmark bookmark = Bookmark.builder()
                .member(member)
                .route(route)
                .markedAt(LocalDateTime.now())
                .build();
        bookmarkRepository.save(bookmark);
    }

    @Transactional
    public void updateName(UpdateRouteNameRequestDTO dto) {
        Member member = getMemberFromJwt();
        Route route = routeRepository.findByIdAndMemberId(dto.getRouteId(), member.getId());
        if (route == null) throw new CustomException(ErrorCode.UNAUTHORIZED_UPDATE_NAME);
        route.updateRouteName(dto.getName());
        routeRepository.save(route);
    }

    public Page<RouteDetailResponseDTO> findBookmark(PageRequestDTO pageRequestDTO) {
        Member member = getMemberFromJwt();
        return routeRepository.findRouteDetailsByMemberId(member.getId(), pageRequestDTO, true);
    }


    public Page<RouteDetailResponseDTO> findMyRoute(PageRequestDTO pageRequestDTO) {
        Member member = getMemberFromJwt();
        return routeRepository.findRouteDetailsByMemberId(member.getId(), pageRequestDTO, false);
    }

    private Member getMemberFromJwt() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        return memberRepository.findByEmail(email).orElseThrow(() -> {
            throw new CustomException(ErrorCode.NO_MEMBER);
        });
    }


}
