package com.elice.tripnote.domain.route.repository;

import com.elice.tripnote.domain.route.entity.Route;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RouteRepository extends JpaRepository<Route, Long>, CustomRouteRepository {
    Route findByIdAndMemberId(Long routeId, Long memberId);
}
