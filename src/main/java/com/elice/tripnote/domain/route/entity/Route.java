package com.elice.tripnote.domain.route.entity;

import com.elice.tripnote.domain.member.entity.Member;
import com.elice.tripnote.domain.spot.constant.Region;
import com.elice.tripnote.global.entity.BaseTimeEntity;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@Getter
@Table(name = "route")
public class Route extends BaseTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", nullable = false)
    private Member member;

    @Column(name = "integrated_routes", nullable = false)
    String integratedRoutes;

    @Enumerated(EnumType.STRING)
    @Column(name = "region", nullable = true)
    Region region;

    @Column(nullable = true)
    private int expense;

    @Column(nullable = true)
    private String name;

//    @Column(nullable = false, name = "likes_Last3Months")
//    private int likesLast3Months;

    @Builder
    public Route(Member member, String integratedRoutes, String name,Region region/*, int likesLast3Months*/) {
        this.member = member;
//        this.expense = expense;
        this.integratedRoutes=integratedRoutes;
        this.name=name;
        this.region=region;
//        this.likesLast3Months=likesLast3Months;
    }


    public void deleteRoute() {
        this.deletedAt = LocalDateTime.now();
    }

    public void updateRouteName(String name) {
        this.name = name;
    }
}
