package com.elice.tripnote.domain.route.repository;

import com.elice.tripnote.domain.hashtag.entity.QHashtag;
import com.elice.tripnote.domain.link.bookmark.entity.QBookmark;
import com.elice.tripnote.domain.link.favorite.entity.QFavorite;
import com.elice.tripnote.domain.link.routespot.entity.QRouteSpot;
import com.elice.tripnote.domain.post.entity.QPost;
import com.elice.tripnote.domain.route.entity.*;
import com.elice.tripnote.domain.spot.constant.Region;
import com.elice.tripnote.domain.spot.entity.QSpot;
import com.elice.tripnote.domain.spot.entity.Spot;
import com.elice.tripnote.global.entity.PageRequestDTO;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class CustomRouteRepositoryImpl implements CustomRouteRepository {
    private final JPAQueryFactory query;
    private final QRoute route = new QRoute("r");
    private final QRouteSpot routeSpot = new QRouteSpot("rs");
    private final QSpot spot = new QSpot("s");
    private final QFavorite favorite = new QFavorite("f");
    private final QBookmark bookmark = new QBookmark("b");
    private final QPost post = new QPost("p");
    private final QHashtag hashtag = new QHashtag("h");

    @PersistenceContext
    private EntityManager em;

    public Page<RouteDetailResponseDTO> findRouteDetailsByMemberId(Long memberId, PageRequestDTO pageRequestDTO, boolean isBookmark) {
        PageRequest pageRequest = PageRequest.of(pageRequestDTO.getPage() - 1, pageRequestDTO.getSize());

        long total = 0;
        List<RouteIdNameResponseDTO> routes = null;
        if (isBookmark) {
            routes = query
                    .select(Projections.constructor(RouteIdNameResponseDTO.class,
                            post.id,
                            route.id,
                            route.name
                    ))
                    .from(route)
                    .join(bookmark).on(bookmark.route.id.eq(route.id))
                    .leftJoin(post).on(post.route.id.eq(route.id))
                    .where(bookmark.member.id.eq(memberId)
                            .and(route.deletedAt.isNull()))
                    .offset(pageRequest.getOffset())
                    .limit(pageRequest.getPageSize())
                    .orderBy(route.id.asc())
                    .fetch();

            total = query
                    .select(route.count())
                    .from(route)
                    .join(bookmark).on(bookmark.route.id.eq(route.id))
                    .leftJoin(post).on(post.route.id.eq(route.id))
                    .where(bookmark.member.id.eq(memberId)
                            .and(route.deletedAt.isNull()))
                    .fetchOne();

        } else {
            routes = query
                    .select(Projections.constructor(RouteIdNameResponseDTO.class,
                            post.id,
                            route.id,
                            route.name
                    ))
                    .from(route)
                    .leftJoin(post).on(post.route.id.eq(route.id))
                    .where(route.member.id.eq(memberId)
                            .and(route.deletedAt.isNull()))
                    .offset(pageRequest.getOffset())
                    .limit(pageRequest.getPageSize())
                    .orderBy(route.id.asc())
                    .fetch();

            total = query
                    .select(route.count())
                    .from(route)
                    .where(route.member.id.eq(memberId)
                            .and(route.deletedAt.isNull()))
                    .fetchOne();
        }

        // 그 중 경로 id만 리스트로 추출
        List<Long> routeIds = routes.stream()
                .map(RouteIdNameResponseDTO::getRouteId)
                .collect(Collectors.toList());

        List<SpotRouteIdDTO> spots = query
                .select(Projections.constructor(SpotRouteIdDTO.class,
                        routeSpot.route.id,
                        spot.id,
                        spot.location,
                        spot.imageUrl,
                        spot.region,
                        spot.address,
                        spot.lat,
                        spot.lng
                ))
                .from(routeSpot)
                .join(spot).on(spot.id.eq(routeSpot.spot.id))
                .where(routeSpot.route.id.in(routeIds))
                // 일단 route id 순서대로, 그 안에서는 sequence 순서대로
                .orderBy(routeSpot.route.id.asc(), routeSpot.sequence.asc())
                .fetch();

        Map<Long, List<SpotRouteIdDTO>> routeIdToSpotsMap = spots.stream()
                .collect(Collectors.groupingBy(SpotRouteIdDTO::getRouteId));

        List<RouteDetailResponseDTO> routeDetails = routes.stream()
                .map(routeDto -> new RouteDetailResponseDTO(
                        routeDto.getRouteId(),
                        routeDto.getPostId(),
                        routeDto.getName(),
                        routeIdToSpotsMap.getOrDefault(routeDto.getRouteId(), List.of())
                                .stream()
                                .map(spotDto -> new Spot(
                                        spotDto.getId(),
                                        spotDto.getLocation(),
                                        spotDto.getImageUrl(),
                                        spotDto.getRegion(),
                                        spotDto.getAddress(),
                                        spotDto.getLat(),
                                        spotDto.getLng()
                                ))
                                .collect(Collectors.toList())
                ))
                .collect(Collectors.toList());


        return new PageImpl<>(routeDetails, pageRequest, total);
    }


    public int getRouteLikeCounts(Long routeId) {
        //해당 루트의 좋아요
        Integer likeCount = query
                .select(favorite.id.count().intValue())
                .from(favorite)
                .where(favorite.route.id.eq(routeId))
                .fetchOne();

        return likeCount != null ? likeCount : 0;
    }

    public boolean findHashtagIdIdCity(Long hashtagId) {

        return query
                .select(hashtag.isCity)
                .from(hashtag)
                .where(hashtag.id.eq(hashtagId))
                .fetchOne();
    }

    public List<RecommendedRouteResponseDTO> getRecommendedRoutes(List<Long> routeIds, Long memberId, boolean isMember) {

        System.out.println("들어오는 route id: " + routeIds);
        List<Tuple> results = query
                .select(
                        route.id,
                        post.id,
                        spot.id,
                        spot.location,
                        spot.imageUrl,
                        spot.region,
                        spot.address,
                        spot.lat,
                        spot.lng,
                        Expressions.as( //메인 쿼리로 필터링된 내용을 기반으로 함
                                JPAExpressions.select(favorite.id.count())
                                        .from(favorite) //메인 쿼리에 조인된 형태?
                                        .where(favorite.route.id.eq(route.id)),
                                "likes"
                        ),
                        Expressions.as(
                                isMember ? JPAExpressions.select(favorite.id.count())
                                        .from(favorite)
                                        .where(favorite.member.id.eq(memberId)
                                                .and(favorite.route.id.eq(route.id)))
                                        : Expressions.constant(0L),
                                "likedAt" //해당 route id에 좋아요를 했는지
                        ),
                        Expressions.as(
                                isMember ? JPAExpressions.select(bookmark.id.count())
                                        .from(bookmark)
                                        .where(bookmark.member.id.eq(memberId)
                                                .and(bookmark.route.id.eq(route.id)))
                                        : Expressions.constant(0L),
                                "markedAt" //해당 route id에 북마크를 했는지
                        )
                )
                .from(route)
                .leftJoin(post).on(post.route.id.eq(route.id))  // post는 null이어도 표시
                .join(routeSpot).on(routeSpot.route.id.eq(route.id))
                .join(spot).on(routeSpot.spot.id.eq(spot.id))
                .where(route.id.in(routeIds))
                .fetch();

        // 결과를 처리하여 DTO 리스트 생성
        Map<Long, RecommendedRouteResponseDTO> resultMap = results.stream()
                .collect(Collectors.groupingBy(
                        tuple -> tuple.get(route.id), //각 튜플의 route id를 key로 정함
                        Collectors.collectingAndThen(Collectors.toList(), tuples -> {

                            // key로 선택된 route id로 먼저 튜플들을 묶고, 그 묶인 튜플들 중 첫번째 튜플에서 값가져옴
                            // 사실 likes, likedAt, markedAt의 값은 위처럼 묶인 튜플들끼리 값이 같기 때문에 아무거나 가져와도 됨
                            int likes = Math.toIntExact((Long) tuples.get(0).get(Expressions.path(Long.class, "likes")));
                            boolean likedAt = tuples.get(0).get(Expressions.path(Long.class, "likedAt")) > 0;
                            boolean markedAt = tuples.get(0).get(Expressions.path(Long.class, "markedAt")) > 0;

                            //여행지 리스트 객체 생성
                            List<Spot> spots = tuples.stream()
                                    .map(tuple -> new Spot(tuple.get(spot.id),
                                            tuple.get(spot.location),
                                            tuple.get(spot.imageUrl),
                                            tuple.get(spot.region),
                                            tuple.get(spot.address),
                                            tuple.get(spot.lat),
                                            tuple.get(spot.lng)))
                                    .collect(Collectors.toList());

                            return new RecommendedRouteResponseDTO(
                                    tuples.get(0).get(route.id),
                                    tuples.get(0).get(post.id),
                                    spots,
                                    likes,
                                    likedAt,
                                    markedAt
                            );
                        })
                ));

        // integratedRouteIds 순서에 맞춰 정렬된 List 생성
        return routeIds.stream()
                .filter(resultMap::containsKey)  // 매핑된 값만 필터링
                .map(resultMap::get)  // 순서대로 매핑된 값 가져오기
                .collect(Collectors.toList());

    }

    public List<Long> findIntegratedRouteIdsBySpotsAndLikes(List<Long> spots) {
//        //모든 여행지를 통과하면서 삭제되지 않은 경로
//        List<Long> allPassRouteIds = query
//                .selectDistinct(route.id)
//                .from(route)
//                .join(routeSpot).on(routeSpot.route.id.eq(route.id))
//                .where(routeSpot.spot.id.in(spots)
//                        .and(route.deletedAt.isNull())
//                        .and(routeSpot.spot.id.countDistinct().eq((long) spots.size())))
//                .fetch();
//
//        LocalDateTime fewMonthsAgo = LocalDateTime.now().minusMonths(3);
//
//        //같은 경로끼리 묶어서 가장 좋아요 많은 순으로 통합 경로 정렬
//        //그리고 각 그룹에서 가장 좋아요가 많은 경로를 리턴
//        return query.select(route.id)
//                .from(route)
//                .join(favorite).on(route.id.eq(favorite.route.id))
//                .where(favorite.likedAt.after(fewMonthsAgo)
//                        .and(route.id.in(allPassRouteIds)))
//                .groupBy(route.integratedRoutes)
//                .orderBy(favorite.id.count().desc())
//                .limit(5)
//                .fetch();


        // 모든 여행지를 통과하면서 삭제되지 않은 경로 찾기
        List<Long> allPassRouteIds = query
                .select(route.id)
                .from(route)
                .join(routeSpot).on(routeSpot.route.id.eq(route.id))
                .where(routeSpot.spot.id.in(spots)
                        .and(route.deletedAt.isNull()))
                .groupBy(route.id)
                .having(routeSpot.spot.id.countDistinct().eq((long) spots.size()))
                .fetch();

        // integratedRoutes 그룹별 좋아요 개수 계산 및 상위 5개 그룹 선택
//        List<Tuple> topIntegratedRoutes = query
//                .select(route.integratedRoutes, favorite.id.count())
//                .from(route)
//                .leftJoin(favorite).on(favorite.route.id.eq(route.id))
//                .where(route.id.in(allPassRouteIds))
//                .groupBy(route.integratedRoutes)
//                .orderBy(favorite.id.count().desc())
//                .limit(5)
//                .fetch();

        // 각 그룹에서 가장 좋아요가 많은 route.id 선택
        LocalDateTime threeMonthsAgo = LocalDateTime.now().minusMonths(3);

        return query
                .select(route.id, route.integratedRoutes, favorite.id.count())
                .from(route)
                .leftJoin(favorite).on(favorite.route.id.eq(route.id).and(favorite.likedAt.after(threeMonthsAgo)))
                .where(route.integratedRoutes.in(
                        JPAExpressions
                                .select(route.integratedRoutes)
                                .from(route)
                                .leftJoin(favorite).on(favorite.route.id.eq(route.id).and(favorite.likedAt.after(threeMonthsAgo)))
                                .where(route.id.in(allPassRouteIds))
                                .groupBy(route.integratedRoutes)
                                .orderBy(favorite.id.count().desc())
                                .limit(5)
                ))
                .groupBy(route.integratedRoutes, route.id)
                .orderBy(route.integratedRoutes.asc(), favorite.id.count().desc())
                .fetch()
                .stream()
                .collect(Collectors.groupingBy(
                        tuple -> tuple.get(route.integratedRoutes),
                        Collectors.mapping(
                                tuple -> tuple.get(route.id),
                                Collectors.toList()
                        )
                ))
                .values()
                .stream()
                .map(list -> list.get(0))
                .collect(Collectors.toList());



    }

    public boolean existsByMemberIdAndRouteId(Long memberId, Long routeId, boolean isLike) {
        if (isLike) {
            return query
                    .select(favorite.id)
                    .from(favorite)
                    .where(favorite.route.id.eq(routeId).and(favorite.member.id.eq(memberId)))
                    .fetchCount() > 0;
        } else {
            return query
                    .select(bookmark.id)
                    .from(bookmark)
                    .where(bookmark.route.id.eq(routeId).and(bookmark.member.id.eq(memberId)))
                    .fetchCount() > 0;
        }

    }

    public void deleteByMemberIdAndRouteId(Long memberId, Long routeId, boolean isLike) {

        if (isLike) {
            query.delete(favorite)
                    .where(favorite.route.id.eq(routeId)
                            .and(favorite.member.id.eq(memberId)))
                    .execute();
        } else {
            query.delete(bookmark)
                    .where(bookmark.route.id.eq(routeId)
                            .and(bookmark.member.id.eq(memberId)))
                    .execute();
        }
        em.flush();
        em.clear();
    }

    public List<Long> findTopRoutesByRegion(Region region) {


        LocalDateTime fewMonthsAgo = LocalDateTime.now().minusMonths(3);

        // 1단계: integratedRoutes 그룹별 좋아요 개수 계산 및 상위 5개 그룹 선택
        List<String> topIntegratedRoutes = query
                .select(route.integratedRoutes)
                .from(route)
                .leftJoin(favorite).on(route.id.eq(favorite.route.id))
                .where(route.region.eq(region)
                        .and(route.deletedAt.isNull())
                        .and(favorite.likedAt.after(fewMonthsAgo).or(favorite.likedAt.isNull())))
                .groupBy(route.integratedRoutes)
                .orderBy(favorite.id.count().desc())
                .limit(5)
                .fetch();

        // 2단계: 각 그룹에서 가장 좋아요가 많은 route.id 선택
        return query
                .select(route.id, route.integratedRoutes, favorite.id.count())
                .from(route)
                .leftJoin(favorite).on(route.id.eq(favorite.route.id))
                .where(route.integratedRoutes.in(topIntegratedRoutes)
                        .and(route.region.eq(region))
                        .and(route.deletedAt.isNull())
                        .and(favorite.likedAt.after(fewMonthsAgo).or(favorite.likedAt.isNull())))
                .groupBy(route.integratedRoutes, route.id)
                .orderBy(route.integratedRoutes.asc(), favorite.id.count().desc())
                .fetch()
                .stream()
                .collect(Collectors.groupingBy(
                        tuple -> tuple.get(route.integratedRoutes),
                        Collectors.mapping(
                                tuple -> tuple.get(route.id),
                                Collectors.toList()
                        )
                ))
                .values()
                .stream()
                .map(list -> list.get(0))
                .collect(Collectors.toList());

    }
}
