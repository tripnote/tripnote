package com.elice.tripnote.domain.discord.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


@Service
@Slf4j
public class WebHookService {

    //rateLimit 초과 10회 이상인 경우 관리자페이지로 메시지 전달 + 디스코드로 메시지 보내기
    @Value("${discord.webhook.url}")
    private String webhookUrl;

    private final WebClient webClient;

    public WebHookService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }
    public Mono<Void> sendDiscordRateLimit(String message) {

        return webClient.post()
            .uri(webhookUrl)
            .bodyValue(new Message(message))
            .retrieve()
            .bodyToMono(Void.class)
            .onErrorResume(e -> {
                log.error("디스코드 메세지 보내는 과정에서 에러 발생: ", e);
                return Mono.empty();
            });
    }

    private static class Message {
        private final String content;

        public Message(String content) {
            this.content = content;
        }

        public String getContent() {
            return content;
        }
    }
}
