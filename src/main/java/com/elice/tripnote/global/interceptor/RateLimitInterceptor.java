package com.elice.tripnote.global.interceptor;

import com.elice.tripnote.domain.discord.service.WebHookService;
import com.elice.tripnote.global.GetClientIP;
import com.elice.tripnote.global.annotation.SearchBucketLimit;
import com.elice.tripnote.global.config.BucketConfig;
import com.elice.tripnote.global.entity.BucketAddTimestamp;
import com.elice.tripnote.global.exception.CustomException;
import com.elice.tripnote.global.exception.ErrorCode;
import io.github.bucket4j.ConsumptionProbe;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
@RequiredArgsConstructor
@Slf4j
public class RateLimitInterceptor implements HandlerInterceptor {

    private final BucketConfig bucketConfig;
    private final WebHookService webHookService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        BucketAddTimestamp bucketToUse;
        String clientIP = GetClientIP.getClientIP(request);

        //SearchBucketLimit 어노테이션의 유무에 따라 searchBucket, defaultBucket을 적용
        if (handlerMethod.hasMethodAnnotation(SearchBucketLimit.class)) {
            log.info("SearchBucketLimit 어노테이션이 존재합니다. IP: {}", clientIP);
            bucketToUse = bucketConfig.getSearchBucket(clientIP);
        } else {
            log.info("SearchBucketLimit 어노테이션이 존재하지 않습니다. IP: {}", clientIP);
            bucketToUse = bucketConfig.getDefaultBucket(clientIP);
        }

        //남은 토큰 정보
        ConsumptionProbe probe = bucketToUse.getBucket().tryConsumeAndReturnRemaining(1);
        bucketToUse.updateLastUpdateTime();

        //요청이 제한되었을 때
        if (!probe.isConsumed()) {

            //1시간동안 10번이상 초과한경우 디스코드 알람을 보냄
            bucketConfig.incrementRateLimitExceededCount(clientIP);
            int exceededCount = bucketConfig.getRateLimitExceededCount(clientIP);

            if(exceededCount >= 10){
                String message = String.format("10회 이상 rate limit 초과한 clientIP: %s", clientIP);
                webHookService.sendDiscordRateLimit(message)
                        .subscribe(
                                null,
                                throwable -> log.error("디스코드 메세지 보내기 실패, throwable")
                        );
            }

            //토큰이 재설정되기 까지 남은 초
            long waitForRefill = probe.getNanosToWaitForRefill() / 1_000_000_000;  //seconds
            log.info("제한된 요청 수를 초과하였습니다. IP: {}, 대기 시간: {} 초", clientIP, waitForRefill);
            //남은 시간 헤더에 추가
            response.setHeader("X-RateLimit-Retry-After", String.valueOf(waitForRefill));
            throw new CustomException(ErrorCode.TOO_MANY_REQUESTS);
        }

        return true;
    }
}
