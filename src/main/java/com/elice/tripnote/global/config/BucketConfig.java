package com.elice.tripnote.global.config;

import com.elice.tripnote.global.entity.BucketAddTimestamp;
import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Refill;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
@Slf4j
public class BucketConfig {

    //ConcurrentHashMap 사용 이유: 여러 요청이 동시에 들어올 수 있는 환경이므로 ConcurrentHashMap를 사용해 동시성 문제를 해결하기 위해 사용함.
    private final Map<String, BucketAddTimestamp> defaultBuckets = new ConcurrentHashMap<>();
    private final Map<String, BucketAddTimestamp> searchBuckets = new ConcurrentHashMap<>();
    private final Map<String, Integer> rateLimitExceededCount = new ConcurrentHashMap<>();

    /**
     * 특정 API에 대한 rate limiting

     * 몇초당 몇번의 api호출을 가능하게 할 것인지 커스텀 가능
     **/
    //@Bean
    //public Bucket customBucket(int count, int time) {

    //    //10초당 1개 요청
    //    //충전 간격을 10초로 지정하며, 한 번 충전할 때마다 1개의 토큰을 충전한다
    //    final Refill refill = Refill.intervally(count, Duration.ofSeconds(time));

    //    //버킷의 크기는 3개
    //    final Bandwidth limit = Bandwidth.classic(count, refill);

    //    return Bucket.builder()
    //            .addLimit(limit)
    //            .build();
    //}

    /**
     * 기본적인 API에 대한 rate limiting

     * 10초당 10번 api 호출 가능

     **/
    @Bean
    public Map<String, BucketAddTimestamp> defaultBuckets() {
        return defaultBuckets;
    }

    /**
     * 검색 API에 대한 rate limiting

     * limit이 발생하였을 때,
     * 5초에 1번 검색이 가능하다는 알림을 보낼 필요 있음

     **/
    @Bean
    public Map<String, BucketAddTimestamp> searchBuckets() {
        return searchBuckets;
    }

    /**
     * clentIP별 rate limit가 초과된 횟수 저장을 위한 Map
     **/
    @Bean
    public Map<String, Integer> rateLimitExceededCount() {
        return rateLimitExceededCount;
    }

    public BucketAddTimestamp getDefaultBucket(String clientIp) {
        return defaultBuckets.computeIfAbsent(clientIp, k -> createBucket(2, 10));
    }

    public BucketAddTimestamp getSearchBucket(String clientIp) {
        return searchBuckets.computeIfAbsent(clientIp, k -> createBucket(1, 5));
    }

    //rate limit 초과된 경우 count 증가
    public void incrementRateLimitExceededCount(String clientIp) {
        rateLimitExceededCount.compute(clientIp, (k, v) -> v == null ? 1 : v + 1);
    }

    public int getRateLimitExceededCount(String clientIp) {
        return rateLimitExceededCount.getOrDefault(clientIp, 0);
    }
    public BucketAddTimestamp createBucket(int count, int second){

        //second당 count번 요청 가능
        final Refill refill = Refill.intervally(count, Duration.ofSeconds(second));

        //버킷의 크기는 count
        final Bandwidth limit = Bandwidth.classic(count, refill);

        return new BucketAddTimestamp(Bucket.builder()
                .addLimit(limit)
                .build());
    }

    //1시간 간격으로 1시간동안 사용되지 않은 버킷 제거  -> 메모리 관리
    @Scheduled(fixedRate = 3600000) // 1시간마다 실행
    public void cleanupBuckets() {
        long now = System.currentTimeMillis();
        defaultBuckets.entrySet().removeIf(entry -> now - entry.getValue().getLastUpdateTime() > 3600000);
        searchBuckets.entrySet().removeIf(entry -> now - entry.getValue().getLastUpdateTime() > 3600000);
        log.info("1시간동안 사용되지 않은 버킷이 제거 되었습니다.");
    }

    @Scheduled(fixedRate = 3600000) // 1시간마다 실행
    public void cleanupRateLimitExceededCount() {
        rateLimitExceededCount.clear();
        log.info("Rate limit 초과 횟수가 초기화되었습니다.");
    }
}