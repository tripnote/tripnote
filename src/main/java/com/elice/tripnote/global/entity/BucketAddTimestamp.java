package com.elice.tripnote.global.entity;

import io.github.bucket4j.Bucket;

public class BucketAddTimestamp {
    private final Bucket bucket;
    private volatile long lastUpdateTime;

    public BucketAddTimestamp(Bucket bucket) {
        this.bucket = bucket;
        this.lastUpdateTime = System.currentTimeMillis();
    }

    public Bucket getBucket() {
        return bucket;
    }

    public long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void updateLastUpdateTime() {
        this.lastUpdateTime = System.currentTimeMillis();
    }
}
