package com.elice.tripnote.domain.member.service;

import com.elice.tripnote.domain.admin.repository.AdminRepository;
import com.elice.tripnote.domain.member.entity.*;
import com.elice.tripnote.domain.member.repository.MemberRepository;
import com.elice.tripnote.global.exception.CustomException;
import com.elice.tripnote.global.exception.ErrorCode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class MemberServiceTest {

    @InjectMocks
    private MemberService memberService;

    @Mock
    private MemberRepository memberRepository;

    @Mock
    private AdminRepository adminRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private Validator validator;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void testSignup_Success() {
        MemberRequestDTO dto = MemberRequestDTO.builder()
                .email("test@example.com")
                .password("password12345")
                .nickname("testuser")
                .build();

        when(memberRepository.existsByEmail(anyString())).thenReturn(false);
        when(memberRepository.existsByNickname(anyString())).thenReturn(false);
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn("encodedPassword");

        assertDoesNotThrow(() -> memberService.signup(dto));

        verify(memberRepository).save(any(Member.class));
    }

    @Test
    void testUpdateProfile_Success() {
        ProfileUpdateDTO dto = ProfileUpdateDTO.builder()
                .newNickname("newNickname")
                .newPassword("newPassword")
                .build();

        Authentication auth = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(auth);
        SecurityContextHolder.setContext(securityContext);
        when(auth.getName()).thenReturn("test@example.com");

        Member member = Member.builder()
                .email("test@example.com")
                .nickname("oldNickname")
                .password("oldPassword")
                .status(Status.ACTIVE)
                .build();

        when(memberRepository.findByEmail("test@example.com")).thenReturn(Optional.of(member));
        when(memberRepository.existsByNickname("newNickname")).thenReturn(false);
        when(bCryptPasswordEncoder.encode("newPassword")).thenReturn("encodedNewPassword");

        assertDoesNotThrow(() -> memberService.updateProfile(dto));

        assertEquals("newNickname", member.getNickname());
        assertEquals("encodedNewPassword", member.getPassword());
        verify(memberRepository).save(member);
    }

    @Test
    void testDeleteMember() {
        Authentication auth = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(auth);
        SecurityContextHolder.setContext(securityContext);
        when(auth.getName()).thenReturn("test@example.com");

        Member member = Member.builder()
                .email("test@example.com")
                .nickname("testuser")
                .status(Status.ACTIVE)
                .build();

        when(memberRepository.findByEmail("test@example.com")).thenReturn(Optional.of(member));

        memberService.deleteMember();

        assertEquals(Status.DELETED_BY_USER, member.getStatus());
        assertNotNull(member.getDeletedAt());
        verify(memberRepository).save(member);
    }

    @Test
    void testAddKakaoInfo() {
        Member member = Member.builder()
                .email("test@example.com")
                .nickname("testuser")
                .build();

        member.addKakaoInfo(12345L);

        assertEquals(12345L, member.getOauthId());
        assertEquals("kakao", member.getOauthType());
    }

    @Test
    void testMemberToDto() {
        Member member = Member.builder()
                .id(1L)
                .email("test@example.com")
                .password("password")
                .nickname("testuser")
                .oauthId(12345L)
                .oauthType("kakao")
                .status(Status.ACTIVE)
                .build();

        MemberResponseDTO dto = member.toDto();

        assertEquals(1L, dto.getId());
        assertEquals("test@example.com", dto.getEmail());
        assertEquals("password", dto.getPassword());
        assertEquals("testuser", dto.getNickname());
        assertEquals(12345L, dto.getOauthId());
        assertEquals("kakao", dto.getOauthType());
        assertEquals(Status.ACTIVE, dto.getStatus());
    }
}

