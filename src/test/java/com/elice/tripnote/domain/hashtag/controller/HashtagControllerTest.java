package com.elice.tripnote.domain.hashtag.controller;

import com.elice.tripnote.domain.hashtag.entity.HashtagDTO;
import com.elice.tripnote.domain.hashtag.entity.HashtagRequestDTO;
import com.elice.tripnote.domain.hashtag.entity.HashtagResponseDTO;
import com.elice.tripnote.domain.hashtag.repository.HashtagRepository;
import com.elice.tripnote.domain.hashtag.service.HashtagService;
import com.elice.tripnote.global.entity.PageRequestDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = HashtagController.class)
class HashtagControllerTest {

    @Autowired
    private MockMvc mvc;   // Http method API 테스트

    @MockBean
    HashtagService hashtagService;

    @MockBean
    HashtagRepository hashtagRepository;

    @Test
    void 전체_해시태그_조회() throws Exception {
        //given
        PageRequestDTO pageRequestDTO = new PageRequestDTO(1, 10, "id", true);
        Pageable pageable = PageRequest.of(pageRequestDTO.getPage(), pageRequestDTO.getSize());

        List<HashtagDTO> hashtags = new ArrayList<>();

        for(int i = 1; i <= 2; i++){
            HashtagDTO hashtagDTO = new HashtagDTO((long) i, i+"도", true, null);
            hashtags.add(hashtagDTO);
        }
        Page<HashtagDTO> page = new PageImpl<>(hashtags, pageable, hashtags.size());

        given(hashtagRepository.customFindAll(any(PageRequestDTO.class))).willReturn(page);
        //when
        ResultActions result = mvc.perform(get("/api/admin/hashtags")
                        .contentType(MediaType.APPLICATION_JSON));
        //then
        result.andExpect(status().isOk())
              .andExpect(content().contentType(MediaType.APPLICATION_JSON))
              .andExpect(jsonPath("$.content[0].id").value(1L))
              .andExpect(jsonPath("$.content[0].name").value("1도"))
              .andExpect(jsonPath("$.content[0].isCity").value(true))
              .andExpect(jsonPath("$.content[0].DeletedAt").value(null));

    }

    @Test
    void 도시여부_해시태그_조회() throws Exception {
        //given
        boolean isCity = true;
        boolean isDelete = false;
        List<HashtagResponseDTO> hashtags = new ArrayList<>();

        for(int i = 1; i <= 2; i++){
            HashtagResponseDTO hashtagDTO = new HashtagResponseDTO((long) i, i+"도", true);
            hashtags.add(hashtagDTO);
        }
        given(hashtagRepository.findByIsCityAndIsDelete(isCity, false)).willReturn(hashtags);

        //when
        ResultActions result = mvc.perform(get("/api/hashtags/isCity")
                .param("isCity", String.valueOf(isCity))
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").value("1도"))
                .andExpect(jsonPath("$[0].isCity").value(true))
                .andExpect(jsonPath("$[0].id").value(2L))
                .andExpect(jsonPath("$[0].name").value("2도"))
                .andExpect(jsonPath("$[0].isCity").value(true));
    }
    @Test
    void 해시태그_생성() throws Exception {
        // given
        HashtagRequestDTO requestDTO = new HashtagRequestDTO("새로운해시태그", true);
        HashtagResponseDTO responseDTO = new HashtagResponseDTO(1L, "새로운해시태그", true);

        given(hashtagService.saveHashtag(requestDTO)).willReturn(responseDTO);

        // when
        ResultActions result = mvc.perform(post("/api/admin/hashtags/create")
                .content("{ \"name\": \"새로운해시태그\", \"isCity\": true }")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value("새로운해시태그"))
                .andExpect(jsonPath("$.isCity").value(true));
    }

    @Test
    void 해시태그_수정() throws Exception {
        // given
        Long id = 1L;
        HashtagRequestDTO requestDTO = new HashtagRequestDTO("수정된해시태그", false);
        HashtagResponseDTO responseDTO = new HashtagResponseDTO(id, "수정된해시태그", false);

        given(hashtagService.updateHashtag(id, requestDTO)).willReturn(responseDTO);

        // when
        ResultActions result = mvc.perform(patch("/api/admin/hashtags/update/{id}", id)
                .content("{ \"name\": \"수정된해시태그\", \"isCity\": false }")
                .contentType(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.name").value("수정된해시태그"))
                .andExpect(jsonPath("$.isCity").value(false));
    }

    @Test
    void 해시태그_삭제_성공() throws Exception {
        // given
        Long id = 1L;

        given(hashtagService.deleteHashtag(id)).willReturn(true);

        // when
        ResultActions result = mvc.perform(delete("/api/admin/hashtags/delete/{id}", id));

        // then
        result.andExpect(status().isNoContent());
    }

    @Test
    void 해시태그_삭제_복구_성공() throws Exception {
        // given
        Long id = 1L;

        given(hashtagService.deleteHashtag(id)).willReturn(false);

        // when
        ResultActions result = mvc.perform(delete("/api/admin/hashtags/delete/{id}", id));

        // then
        result.andExpect(status().isOk());
    }
}