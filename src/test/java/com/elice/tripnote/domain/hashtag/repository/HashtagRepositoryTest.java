package com.elice.tripnote.domain.hashtag.repository;

import com.elice.tripnote.domain.hashtag.entity.Hashtag;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE) // test 데이터베이스 사용
@TestPropertySource(properties = { "spring.config.location=classpath:application-test.yaml" })
public class HashtagRepositoryTest {

    @Autowired
    private HashtagRepository hashtagRepository;

    @BeforeEach
    public void setup() {
        Hashtag hashtag1 = Hashtag.builder()
                .name("충북")
                .isCity(true).build();
        Hashtag hashtag2 = Hashtag.builder()
                .name("충남")
                .isCity(true).build();
        Hashtag hashtag3 = Hashtag.builder()
                .name("1인여행")
                .isCity(false).build();
        Hashtag hashtag4 = Hashtag.builder()
                .name("가성비여행")
                .isCity(false).build();

        hashtagRepository.saveAll(List.of(hashtag1, hashtag2, hashtag3, hashtag4));
    }

    @Test
    public void 지역_해시태그_조회_테스트() {
        //given
        //when
        Boolean findHashtag1 = hashtagRepository.existsByName("충북");
        Boolean findHashtag2 = hashtagRepository.existsByName("서울");

        //then
        Assertions.assertThat(findHashtag1).isEqualTo(1);
        Assertions.assertThat(findHashtag2).isEqualTo(0);

    }
}