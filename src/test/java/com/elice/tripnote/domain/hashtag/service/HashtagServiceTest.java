package com.elice.tripnote.domain.hashtag.service;

import com.elice.tripnote.domain.hashtag.entity.Hashtag;
import com.elice.tripnote.domain.hashtag.entity.HashtagRequestDTO;
import com.elice.tripnote.domain.hashtag.entity.HashtagResponseDTO;
import com.elice.tripnote.domain.hashtag.repository.HashtagRepository;
import com.elice.tripnote.global.exception.CustomException;
import com.elice.tripnote.global.exception.ErrorCode;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HashtagServiceTest {

    @InjectMocks
    private HashtagService hashtagService;

    @Mock
    private HashtagRepository hashtagRepository;

    @Test
    void 해시태그_저장(){
        //Given
        String name = "해시태그명";
        boolean isCity = true;
        HashtagRequestDTO requestDTO = new HashtagRequestDTO(name, isCity);

        //중복체크
        when(hashtagRepository.existsByName(requestDTO.getName())).thenReturn(false);
        //when
        HashtagResponseDTO savedHashtag = hashtagService.saveHashtag(requestDTO);

        //Then
        Assertions.assertThat(savedHashtag).isNotNull();
        Assertions.assertThat(name).isEqualTo(savedHashtag.getName());
        Assertions.assertThat(isCity).isEqualTo(savedHashtag.isCity());

    }

    @Test
    void 해시태그명_중복_예외처리() {
        // Given
        String duplicateName = "duplicate";
        HashtagRequestDTO requestDTO = new HashtagRequestDTO(duplicateName, true);

        // 중복체크
        when(hashtagRepository.existsByName(duplicateName)).thenReturn(true);

        // When
        CustomException exception = assertThrows(CustomException.class, () -> {
            hashtagService.saveHashtag(requestDTO);
        });

        // Then
        Assertions.assertThat(exception.getErrorCode()).isEqualTo(ErrorCode.DUPLICATE_NAME);
    }
    @Test
    void 해시태그_수정(){
        //Given
        Long id = 1L;
        HashtagRequestDTO requestDTO = new HashtagRequestDTO("수정해시태그명", false);
        Hashtag existingHashtag = new Hashtag("기존해시태그명", true);
        when(hashtagRepository.getById(id)).thenReturn(existingHashtag);

        //when
        HashtagResponseDTO updatedHashtag = hashtagService.updateHashtag(id, requestDTO);

        //then
        Assertions.assertThat(updatedHashtag).isNotNull();
        Assertions.assertThat(updatedHashtag.getId()).isEqualTo(id);
        Assertions.assertThat(updatedHashtag.getName()).isEqualTo(requestDTO.getName());
        Assertions.assertThat(updatedHashtag.isCity()).isEqualTo(requestDTO.isCity());
    }

    @Test
    void 해시태그_삭제() {
        // Given
        Long id = 1L;
        Hashtag existingHashtag = new Hashtag("해시태그명", true);
        when(hashtagRepository.getById(id)).thenReturn(existingHashtag);

        // When
        boolean isDeleted = hashtagService.deleteHashtag(id);

        // Then
        Assertions.assertThat(isDeleted).isTrue(); // Assuming delete() method sets deletedAt to non-null
        Assertions.assertThat(existingHashtag.getDeletedAt()).isNotNull();
    }

}