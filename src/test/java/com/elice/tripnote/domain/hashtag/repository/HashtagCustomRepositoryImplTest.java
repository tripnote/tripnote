package com.elice.tripnote.domain.hashtag.repository;

import com.elice.tripnote.domain.hashtag.entity.Hashtag;
import com.elice.tripnote.domain.hashtag.entity.HashtagDTO;
import com.elice.tripnote.domain.hashtag.entity.HashtagResponseDTO;
import com.elice.tripnote.domain.hashtag.entity.QHashtag;
import com.elice.tripnote.global.config.QueryDSLConfig;
import com.elice.tripnote.global.entity.PageRequestDTO;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import javax.sql.DataSource;

@DataJpaTest
@Import({QueryDSLConfig.class, HashtagCustomRepositoryImpl.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE) // test 데이터베이스 사용
@TestPropertySource(properties = { "spring.config.location=classpath:application-test.yaml" })
class HashtagCustomRepositoryImplTest {
    @PersistenceContext
    EntityManager em;
    @Autowired
    HashtagRepository hashtagRepository;

    @Autowired
    @Qualifier("hashtagCustomRepositoryImpl")
    HashtagCustomRepository hashtagCustomRepository;

    @Autowired
    DataSource dataSource;

    JPAQueryFactory query;

    @BeforeEach
    void init() {
        query = new JPAQueryFactory(em);

        // 데이터베이스 연결 정보 로그 출력
//        try (Connection connection = dataSource.getConnection()) {
//            DatabaseMetaData metaData = connection.getMetaData();
//            System.out.println("URL: " + metaData.getURL());
//            System.out.println("User: " + metaData.getUserName());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

        hashtagRepository.deleteAll();

        hashtagRepository.saveAll(List.of(
                createHashtag("충북", true),
                createHashtag("충남", true),
                createHashtag("가성비여행", false)
        ));

        em.flush();   //영속성 컨텍스트의 변경사항을 db에 즉시 반영
        em.clear();   //영속성 컨텍스트를 비워 모든 엔티티를 준영속 상태로 만듦

    }

    @Test
    void QueryDSL_Test() {

        //given
        QHashtag hashtag = new QHashtag("hashtag");

        //when
        Hashtag result1 = query.select(hashtag)
                .from(hashtag)
                .where(hashtag.name.eq("충북"))
                .fetchOne();
        Hashtag result2 = query.select(hashtag)
                .from(hashtag)
                .where(hashtag.name.eq("충남"))
                .fetchOne();
        Hashtag result3 = query.select(hashtag)
                .from(hashtag)
                .where(hashtag.name.eq("가성비여행"))
                .fetchOne();

        //then
        Assertions.assertThat(result1.getName()).isEqualTo("충북");
        Assertions.assertThat(result2.isCity()).isEqualTo(true);
        Assertions.assertThat(result3.getDeletedAt()).isNull();

    }

    @Test
    void 해시태그_도시여부_삭제여부_검색_테스트() {
        // Given
        boolean isCity = true;
        boolean isDelete = false;

        // When
        List<HashtagResponseDTO> hashtags = hashtagCustomRepository.findByIsCityAndIsDelete(isCity, isDelete);

        // Then
        Assertions.assertThat(hashtags).isNotNull();
        Assertions.assertThat(hashtags).isNotEmpty();
        System.out.println(hashtags);
    }

    @Test
    void 전체_해시태그_조회_테스트() {
        // Given
        PageRequestDTO pageRequestDTO = new PageRequestDTO(1, 10,"id",true);

        // When
        Page<HashtagDTO> page = hashtagCustomRepository.customFindAll(pageRequestDTO);

        // Then
        Assertions.assertThat(page).isNotNull();
        Assertions.assertThat(page.getTotalElements()).isGreaterThanOrEqualTo(0); // 예상되는 총 개수에 따라 수정
        Assertions.assertThat(page.getContent()).isNotEmpty(); // 결과가 비어 있지 않아야 함
        Assertions.assertThat(page.getContent().get(0)).isInstanceOf(HashtagDTO.class); // DTO 클래스 검증
    }

    private Hashtag createHashtag(String name, boolean isCity) {
        return Hashtag.builder()
                .name(name)
                .isCity(isCity)
                .build();
    }
}