package com.elice.tripnote.domain.route.service;

import com.elice.tripnote.domain.hashtag.entity.Hashtag;
import com.elice.tripnote.domain.hashtag.repository.HashtagRepository;
import com.elice.tripnote.domain.link.routehashtag.entity.RouteHashtag;
import com.elice.tripnote.domain.link.routehashtag.repository.RouteHashtagRepository;
import com.elice.tripnote.domain.member.entity.Member;
import com.elice.tripnote.domain.member.repository.MemberRepository;
import com.elice.tripnote.domain.route.entity.Route;
import com.elice.tripnote.domain.route.entity.SaveRequestDTO;
import com.elice.tripnote.domain.route.repository.RouteRepository;
import com.elice.tripnote.domain.spot.constant.Region;
import com.elice.tripnote.domain.spot.entity.Spot;
import com.elice.tripnote.domain.spot.entity.SpotRegionDTO;
import com.elice.tripnote.domain.spot.repository.SpotRepository;
import com.elice.tripnote.global.exception.CustomException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RouteServiceTest {
    @Mock
    private RouteRepository routeRepository;

    @Mock
    private MemberRepository memberRepository;

    @Mock
    private HashtagRepository hashtagRepository;

    @Mock
    private SpotRepository spotRepository;

    @Mock
    private RouteHashtagRepository routeHashtagRepository;

    @InjectMocks
    private RouteService routeService;

    private SaveRequestDTO requestDto;
    private Member member;
    private Route route;
    private Hashtag hashtag1;
    private Hashtag hashtag2;
    private Long hashtagId1;
    private Long hashtagId2;
    private Long memberId;
    private Long routeId;

    @BeforeEach
    void setUp() {
        requestDto = SaveRequestDTO.builder()
                .spotIds(Arrays.asList(1L, 2L, 3L))
                .hashtagIds(Arrays.asList(4L, 5L))
                .name("Test Route")
                .build();

        member = memberRepository.save(Member.builder()
                .email("test@example.com")
                .build());
        memberId = member.getId();

        route = routeRepository.save(Route.builder()
                .member(member)
                .build());
        routeId = route.getId();

        hashtag1 = hashtagRepository.save(Hashtag.builder()
                .name("SEOUL")
                .isCity(true)
                .build());
        hashtag2 = hashtagRepository.save(Hashtag.builder()
                .name("가족과 함께")
                .isCity(false)
                .build());
        hashtagId1 = hashtag1.getId();
        hashtagId2 = hashtag2.getId();

        Authentication authentication = mock(Authentication.class);
        when(authentication.getName()).thenReturn("test@example.com");
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterEach
    void initiate() {
        routeRepository.deleteAll();
        memberRepository.deleteAll();
    }

    @Test
    @DisplayName("새로운 경로를 저장한다.")
    void testSave() {
        // Arrange
        when(memberRepository.findByEmail(anyString())).thenReturn(Optional.of(member));
        when(spotRepository.getRegionByspotId(any(Long.class))).thenReturn(new SpotRegionDTO(Region.SEOUL));
        when(routeRepository.save(any(Route.class))).thenAnswer(invocation -> invocation.getArgument(0));

        when(hashtagRepository.findById(hashtagId1)).thenReturn(Optional.of(hashtag1));
        when(hashtagRepository.findById(hashtagId2)).thenReturn(Optional.of(hashtag2));

        // Act
        Long routeId = routeService.save(requestDto);

        // Assert
        assertNotNull(routeId);
        verify(routeRepository, times(1)).save(any(Route.class));
        verify(routeHashtagRepository, times(2)).save(any(RouteHashtag.class));
    }

    @Test
    @DisplayName("경로를 삭제한다.")
    void testDeleteRoute() {
        // Arrange
        Long routeId = 1L;

        when(memberRepository.findByEmail(anyString())).thenReturn(Optional.of(member));
        when(routeRepository.findById(routeId)).thenReturn(Optional.of(route));
        when(routeRepository.save(any(Route.class))).thenAnswer(invocation -> invocation.getArgument(0));

        // Act
        Long deletedRouteId = routeService.deleteRoute(routeId);

        // Assert
        assertNotNull(deletedRouteId);
        assertEquals(routeId, deletedRouteId);
        verify(memberRepository, times(1)).findByEmail(anyString());
        verify(routeRepository, times(1)).findById(routeId);
        verify(routeRepository, times(1)).save(any(Route.class));
    }

    @Test
    @DisplayName("경로를 삭제하려하는 사람의 member 객체를 db에서 찾지 못한 경우")
    void testDeleteRoute_NoMember() {
        // Arrange
        Long routeId = 1L;

        when(memberRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(CustomException.class, () -> routeService.deleteRoute(routeId));
        verify(memberRepository, times(1)).findByEmail(anyString());
        verify(routeRepository, times(0)).findById(routeId);
    }

    @Test
    @DisplayName("삭제하려는 경로를 db에서 찾지 못한 경우")
    void testDeleteRoute_NoRoute() {
        // Arrange
        Long routeId = 1L;

        when(memberRepository.findByEmail(anyString())).thenReturn(Optional.of(member));
        when(routeRepository.findById(routeId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(CustomException.class, () -> routeService.deleteRoute(routeId));
        verify(memberRepository, times(1)).findByEmail(anyString());
        verify(routeRepository, times(1)).findById(routeId);
    }

    @Test
    @DisplayName("다른 사람의 경로를 삭제하려 하는 경우")
    void testDeleteRoute_Unauthorized() {
        // Arrange
        Long routeId = 1L;
        Member anotherMember = Member.builder().id(2L).email("another@example.com").build();
        Route anotherRoute = Route.builder().id(routeId).member(anotherMember).build();

        when(memberRepository.findByEmail(anyString())).thenReturn(Optional.of(member));
        when(routeRepository.findById(routeId)).thenReturn(Optional.of(anotherRoute));

        // Act & Assert
        assertThrows(CustomException.class, () -> routeService.deleteRoute(routeId));
        verify(memberRepository, times(1)).findByEmail(anyString());
        verify(routeRepository, times(1)).findById(routeId);
    }
}
