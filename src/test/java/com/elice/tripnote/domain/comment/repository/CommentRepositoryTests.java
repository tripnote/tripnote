package com.elice.tripnote.domain.comment.repository;


import com.elice.tripnote.domain.comment.entity.Comment;
import com.elice.tripnote.domain.comment.entity.CommentResponseDTO;
import com.elice.tripnote.domain.member.entity.Member;
import com.elice.tripnote.domain.member.repository.MemberRepository;
import com.elice.tripnote.domain.post.entity.Post;
import com.elice.tripnote.domain.post.repository.PostRepository;
import com.elice.tripnote.domain.route.entity.Route;
import com.elice.tripnote.domain.route.repository.RouteRepository;
import com.elice.tripnote.global.config.QueryDSLConfig;
import com.elice.tripnote.global.entity.PageRequestDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Import(QueryDSLConfig.class)
public class CommentRepositoryTests {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private MemberRepository memberRepository;

    private Long memberId1;
    private Long memberId2;
    private Long postId1;
    private Long postId2;
    private Long commentId1;
    private Long commentId2;
    private Long commentId3;

    @BeforeEach
    void setup(){

        Member member1 = memberRepository.save(Member.builder()
                .email("test1@tripnote.site")
                .nickname("test1")
                .password("12345678").build());
        Member member2 = memberRepository.save(Member.builder()
                .email("test2@tripnote.site")
                .nickname("test2")
                .password("12345678").build());
        memberId1 = member1.getId();
        memberId2 = member2.getId();

        Route route1 = routeRepository.save(Route.builder()
                .member(member1)
                .integratedRoutes("test1IntergratedRoutes")
                .build());
        Route route2 = routeRepository.save(Route.builder()
                .member(member2)
                .integratedRoutes("test2IntergratedRoutes")
                .build());

        Post post1 = postRepository.save(Post.builder()
                .member(member1)
                .route(route1)
                .title("test1Title")
                .content("test1Content").build());

        Post post2 = postRepository.save(Post.builder()
                .member(member2)
                .route(route2)
                .title("test2Title")
                .content("test2Content").build());

        postId1 = post1.getId();
        postId2 = post2.getId();


        Comment comment1 = commentRepository.save(Comment.builder()
                .content("test1comment")
                .member(member1)
                .post(post1).build());

        Comment comment2 = Comment.builder()
                .content("test2comment")
                .member(member1)
                .post(post2).build();
        comment2.delete();

        Comment comment3 = commentRepository.save(Comment.builder()
                .content("test3comment")
                .member(member2)
                .post(post2).build());

        commentId1 = comment1.getId();
        commentId2 = commentRepository.save(comment2).getId();
        commentId3 = comment3.getId();

    }

    @AfterEach
    void initiate(){
        postRepository.deleteAll();
        routeRepository.deleteAll();
        memberRepository.deleteAll();
    }

    @Test
    @DisplayName("customFindNotDeletedComment")
    void test1(){


        CommentResponseDTO commentResponseDTO = commentRepository.customFindNotDeletedComment(commentId1);

        assertThat(commentResponseDTO)
                .isNotNull()
                .satisfies(dto -> {
                    assertThat(dto.getId()).isEqualTo(commentId1);
                    assertThat(dto.getContent()).isEqualTo("test1comment");
                    assertThat(dto.getNickname()).isEqualTo("test1");
                    assertThat(dto.getReport()).isEqualTo(0);
                    assertThat(dto.getDeletedAt()).isNull();
                });

        commentResponseDTO = commentRepository.customFindNotDeletedComment(commentId2);

        assertThat(commentResponseDTO)
                .isNull();


    }







    @Test
    @DisplayName("customFindNotDeletedCommentsByPostId")
    void test2(){


        Page<CommentResponseDTO> commentResponseDTOList = commentRepository.customFindNotDeletedCommentsByPostId(postId1, new PageRequestDTO() );

        assertThat(commentResponseDTOList)
                .isNotNull();
        assertThat(commentResponseDTOList.getTotalElements())
                .isEqualTo(1);
        assertThat(commentResponseDTOList.getContent().get(0))
                .satisfies(dto -> {
                    assertThat(dto.getId()).isEqualTo(commentId1);
                    assertThat(dto.getContent()).isEqualTo("test1comment");
                    assertThat(dto.getNickname()).isEqualTo("test1");
                    assertThat(dto.getReport()).isEqualTo(0);
                    assertThat(dto.getDeletedAt()).isNull();
                });

        commentResponseDTOList = commentRepository.customFindNotDeletedCommentsByPostId(postId2, new PageRequestDTO() );

        assertThat(commentResponseDTOList.getContent().size())
                .isEqualTo(1);


    }




    @Test
    @DisplayName("customFindComments_commentId")
    void test3(){


        Page<CommentResponseDTO> commentResponseDTOList = commentRepository.customFindComments(commentId1, new PageRequestDTO() );

        assertThat(commentResponseDTOList)
                .isNotNull();
        assertThat(commentResponseDTOList.getTotalElements())
                .isEqualTo(2);
        assertThat(commentResponseDTOList.getContent().get(0))
                .satisfies(dto -> {
                    assertThat(dto.getId()).isEqualTo(commentId1);
                    assertThat(dto.getContent()).isEqualTo("test1comment");
                    assertThat(dto.getNickname()).isEqualTo("test1");
                    assertThat(dto.getReport()).isEqualTo(0);
                    assertThat(dto.getDeletedAt()).isNull();
                });

        commentResponseDTOList = commentRepository.customFindComments(commentId2, new PageRequestDTO() );

        assertThat(commentResponseDTOList.getContent().size())
                .isEqualTo(2);

        commentResponseDTOList = commentRepository.customFindComments(commentId3, new PageRequestDTO() );

        assertThat(commentResponseDTOList.getContent().size())
                .isEqualTo(1);


        commentResponseDTOList = commentRepository.customFindComments(5L, new PageRequestDTO() );

        assertThat(commentResponseDTOList.getContent().size())
                .isEqualTo(3);


    }


    @Test
    @DisplayName("customFindComments_nickname")
    void test4(){


        Page<CommentResponseDTO> commentResponseDTOList = commentRepository.customFindComments("test1", new PageRequestDTO() );

        assertThat(commentResponseDTOList)
                .isNotNull();
        assertThat(commentResponseDTOList.getTotalElements())
                .isEqualTo(2);
        assertThat(commentResponseDTOList.getContent().get(0))
                .satisfies(dto -> {
                    assertThat(dto.getId()).isEqualTo(commentId1);
                    assertThat(dto.getContent()).isEqualTo("test1comment");
                    assertThat(dto.getNickname()).isEqualTo("test1");
                    assertThat(dto.getReport()).isEqualTo(0);
                    assertThat(dto.getDeletedAt()).isNull();
                });

        commentResponseDTOList = commentRepository.customFindComments("test2", new PageRequestDTO() );

        assertThat(commentResponseDTOList.getContent().size())
                .isEqualTo(1);

        commentResponseDTOList = commentRepository.customFindComments("test3", new PageRequestDTO() );

        assertThat(commentResponseDTOList.getContent().size())
                .isEqualTo(0);



    }


}
